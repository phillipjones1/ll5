<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    /**
    * Fillabel fields for an Article.
    *
    * @var array
    */
    protected $fillable = [
        'title',
        'body',
        'published_at',
        'user_id',  //temporary!!    
    ];

    protected $dates = ['published_at'];

    public function scopePublished($query)
    {

        $query->where('published_at','<=', Carbon::now());

    }

        public function scopeUnpublished($query)
        {

        $query->where('published_at','>', Carbon::now());

    }

    public function setPublishedAtAttribute($date)
    {

        $this->attributes['published_at']= Carbon::parse($date);


    }
    /**
     * An article is owned by a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {

        return $this->belongsTo('App\User');

    }
}
