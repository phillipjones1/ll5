<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use Carbon\Carbon;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth;

class ArticlesController extends Controller
{   

    public function __construct()
{
    $this->middleware('auth');
}
    /**
    *   Show all articles
    *
    *   @return Response
    */

    public function index() {

        $articles = Article::latest('published_at')->published()->get();

        return view('articles.index', compact('articles'));
    }

    /**
    *   Show a single article
    *
    *   @param  integer $id
    *   @return Response
    */
    public function show($id) {

        $article = Article::findOrFail($id);

        //dd($article->published_at);
                
        return view('articles.show', compact('article'));
    }

    public function create(){

        return view('articles.create');
    }

    /**
    *   Save a new article
    *
    *   @return Response
    */
    public function store(ArticleRequest $request){

        //Auth::user();
        
        $article = new Article($request->all());

        Auth::user()->articles()->save($article);

        Session::flash('flash_mssage', 'Article successfully published');

        return redirect('articles');
    }

    /**
    *   Edit an article
    *
    *   @return Response
    */
    public function edit($id){

        $article = Article::findOrFail($id);
        return view('articles.edit', compact('article'));

    }

    /**
    *   Update an article
    *
    *   @params integer $id 
    *   @return Response
    */
    public function update($id, ArticleRequest $request){

        $article = Article::findOrFail($id);

        $article->update($request->all());

        return redirect('articles');

    }
}
