<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $fillable[

        'number',
        'to_city',
        'from_city'
    ];
}
