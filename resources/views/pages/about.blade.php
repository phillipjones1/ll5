@extends('layouts.master')

@section('title')
<title>About Me</title>
@stop

@section('content')
<div class="title">About Me: {{ $first }} {{ $last }} </div>
<p> Star Wars is an American epic space opera franchise centered on a film series created by George Lucas. The franchise depicts a galaxy described as "far, far away" in the distant past, and portrays adventures and battles between good and evil.</br></br>The first film in the series, Star Wars (A New Hope), was released on May 25, 1977 by 20th Century Fox, and became a worldwide pop culture phenomenon. It was followed by two sequels, released in 1980 and 1983. A prequel trilogy of films were later released between 1999 and 2005. Reaction to the original trilogy was largely positive, while the prequel trilogy received a more mixed reaction from critics and fans. All six films were nominated for or won Academy Awards, and all were box office successes; the overall box office revenue generated totals $4.38 billion, making Star Wars the fifth-highest-grossing film series. The series has spawned an extensive media franchise – the Expanded Universe – including books, television series, computer and video games, and comic books, resulting in significant development of the series's fictional universe.</p>    
@stop