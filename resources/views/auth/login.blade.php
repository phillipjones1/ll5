@extends('layouts.masterAuth')

@section('title')
<title>Login</title>
@stop

@section('content')
<!-- resources/views/auth/login.blade.php -->

<form method="POST" action="/auth/login">
    {!! csrf_field() !!}

    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Password
        <input type="password" name="password" id="password">
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div>
        <button type="submit">Login</button>
        <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
    </div>
    <div>
    <a class="btn" href="{{ url('/auth/register') }}">Register</a>
    </div>
</form>
@stop