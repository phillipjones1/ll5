@extends('layouts.master')

@section('title')
<title>Edit an Article</title>

@stop 

@section('content')
    @include('partials.errors')

    <h1>Edit: {!! $article->title !!}</h1>
    {!! Form::model($article,['method' => 'PATCH', 'action' => ['ArticlesController@update', $article->id]]) !!}

    @include('partials.form',['submitButtonText'=>'Update Article'])


    {!! Form::close() !!}
    @stop