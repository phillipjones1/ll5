@extends('layouts.master')

@section('content')

<h1>{{ $article->title}}</h1>

        <article>
            <h2>{{ $article->body }}</h2>
        </article>
        <form action="/articles">
            <input type="submit" value="Main Page">
        </form>
        <form action="{{ route('articles.edit', $article->id) }}">
            <input type="submit" value="Edit Article">
        </form>
{{--         
        <div class="form-group">
            {!! Form::submit($submitButtonText,['class' => 'form-control']) !!}
        </div> 
--}}
@stop

@section('footer')
    <p>Phillip made this app</p>

@stop

