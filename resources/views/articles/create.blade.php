@extends('layouts.master')

@section('title')
<title>Create an Article</title>

@stop

@section('content')

<h1>Write a New Article</h1>
<hr/>

@include('partials.errors')
{!! Form::open(['url' => '/articles']) !!}

@include('partials.form',['submitButtonText'=>'Add Article'])

        
{!! Form::close() !!}


@stop

@section('footer')


@stop

